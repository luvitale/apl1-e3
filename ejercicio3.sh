#!/bin/bash
usage(){
	echo "Uso: $0 -d directorio [-o directorio_resultado] [-u umbral]" 
}

ayuda(){
	echo	
	usage
	echo
	echo "-d “directorio”: path absoluto o relativo a analizar. Requerido."
	echo
	echo "-o “directorio_resultado”: path absoluto o relativo del directorio donde se generará el archivo de salida. Opcional. Si no se informa se generará en el directorio de ejecución."
	echo
	echo "-u “umbral”: tamaño definido en KB para definir el umbral a analizar. Opcional. Si no es indicado, se considerará como umbral el promedio de peso de los archivos inspeccionados."

	echo
	echo "---"
	echo

	echo "Genera un informe de los archivos duplicados en el filesystem y de los que superan en tamaño un umbral determinado."
	echo
	echo "Para los duplicados se indica en una tabla:"
	echo -e "\t- Nombre del archivo"
	echo -e "\t- Ruta relativa de los directorios donde se encuentra"
	echo -e "\t- Fecha de última modificación"
	echo "Cada archivo duplicado diferente está separado por un espacio en blanco con respecto al otro."
	echo
	echo "Para los que superan en tamaño un umbral se indica en otra tabla:"
	echo -e "\t- Ruta absoluta"
	echo -e "\t- Tamaño en kB"
	echo "Se ordenan por tamaño en orden descendente."
	echo
	echo "-----------------------------------------------------------------"
	echo "|      Las tablas se ubican una debajo de la otra y poseen      |"
	echo "|     un encabezado explicando que significa cada una de las    |"
	echo "|     columnas que a su vez están separadas por un tabulador    |"
	echo "-----------------------------------------------------------------"
	echo
	echo "-----------------------------------------------------------------"
	echo "|   Los resultados se muestran por pantalla y se guardan en un  |"
	echo "|   archivo cuyo nombre es resultado_{yyyy-mm-dd_hh:mm:ss}.out  |"
	echo "-----------------------------------------------------------------"
	echo
}

archivosDuplicados(){
	local directorio="$1"
	tablaDuplicados=$(find "$directorio" -type f -printf '%h\t%TY-%Tm-%Td_%Tk:%TM:%.2TS\t%f\n' |
		sort -t$'\t' -k3 |
		awk -F $'\t' '{dir=$1; gsub(/ /, ":", dir); print dir"\t"$2"\t"$3}' |
		uniq -f2 --all-repeated=separate |
		awk -F $'\t' 'BEGIN {
			print "Nombre del archivo duplicado\tDirectorios donde se encuentra\tFecha de modificación\n"
		} {dir=$1; gsub(":", " ", dir); print $3"\t"dir"\t"$2}')
	cantidad_filas=$(echo "$tablaDuplicados" | wc -l)
	if [ $cantidad_filas -gt 1 ]
	then
		echo "$tablaDuplicados"
	else
		echo "No hay archivos duplicados."
	
	fi
}

promedioDePeso(){
	directorio="$1"
	peso_total=$(ls -lR --block-size=KB "$directorio" |
		grep '^-' |
		awk '{total += $5} END {print total}')
	cantidad_archivos=$(find "$directorio" -type f | wc -l)
	if [ $cantidad_archivos -ne 0 ]
	then
		echo "scale=3; $peso_total/$cantidad_archivos" | bc
	fi
}

buscarSuperanUmbral(){
	local directorio="$1"
	local umbral="$2"
	find "$directorio" -type f | while read file
	do
		peso=$(ls -l --block-size=KB "$file" | awk '{size=$5; gsub("kB", "", size); print size}')
		
		if (( $(echo "$peso > $umbral" | bc -l ) ))
		then
			echo -e "$(realpath "$file")\t$peso"
		fi
	done
}

archivosQueSuperanUmbral(){
	local directorio="$1"
	if [ -z "$2" ]
	then
		umbral=$(promedioDePeso "$directorio")
	else
		umbral="$2"
	fi

	tablaSuperanUmbral=$(buscarSuperanUmbral "$directorio" "$umbral")

	if [ -z "$tablaSuperanUmbral" ]
	then
		echo "No hay archivos que superen el umbral."
	else
		echo "$tablaSuperanUmbral" | sort -k2 -r | awk -F $'\t' 'BEGIN { print "Path del archivo que supera umbral\tTamaño" } {print $1"\t"$2"kB"}'
	fi
}

main(){
	directorio_resultado="./"
	while getopts ":d:o:u:h-:" OPT
	do
		if [ "$OPT" = "-" ]; then   # long option: reformulate OPT and OPTARG
    			OPT="${OPTARG%%=*}"       # extract long option name
		        OPTARG="${OPTARG#$OPT}"   # extract long option argument (may be empty)
			OPTARG="${OPTARG#=}"      # if long option argument, remove assigning `=`
		fi
		
		case "$OPT" in
			d)
				directorio=${OPTARG%%/}${OPTARG:+/}
				#echo "Directorio: $directorio"
				;;
			o)
				directorio_resultado=${OPTARG%%/}${OPTARG:+/}
				#echo "Directorio resultado: $directorio_resultado"
				if [ ! -d "$directorio_resultado" ]
				then
					echo "El directorio para los resultados no es válido"
					usage
					exit 3
				fi
				;;
			u)
				umbral=${OPTARG}
				#echo "Umbral: $umbral"
				re='^[0-9]+$'
				if ! [[ $umbral =~ $re ]]
				then
					echo "Debe ingresarse un número entero como umbral" >&2
					exit 4
				fi
				;;
			h | help)
				ayuda
				exit 5
				;;
			*)
				usage
				exit 6
				;;
		esac
	done

	if [ -z "$directorio" ]
	then
		echo "Se debe ingresar un directorio"
		usage
		exit 1
	elif [ ! -d "$directorio" ]
	then
		echo "El directorio ingresado no es válido"
		usage
		exit 2
	fi

	info_de_archivos_duplicados=$(archivosDuplicados "$directorio")

	info_de_archivos_que_superan_umbral=$(archivosQueSuperanUmbral "$directorio" "$umbral")

	archivo_resultado="resultado_{$(date +%F_%T)}.out"
	
	resultado=$(echo -e "$info_de_archivos_duplicados\n\n$info_de_archivos_que_superan_umbral")

	echo -e "$resultado"

	echo -e "$resultado" > $directorio_resultado$archivo_resultado
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]
then
	main "$@"
fi
