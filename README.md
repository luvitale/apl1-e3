# Ejercicio 3 de la APL 1

Para disminuir el espacio ocupado en un disco rígido, se desea determinar la existencia de archivos duplicados en el File System y también cuáles archivos superan en tamaño un umbral determinado.
Para ello se necesita crear un script que recorra recursivamente un directorio y genere un informe sobre los archivos duplicados y los que superan el umbral.

Para el caso de archivos duplicados se deberá indicar la siguiente información:
- El nombre del archivo.
- Los directorios donde se encuentra dicho archivo.
- La fecha de última modificación de cada uno.

Para el caso de los archivos que superen el umbral de tamaño, se deberá indicar el path completo del archivo y el tamaño del mismo, ordenados de forma descendente.

**Parámetros (el orden de los parámetros no debe ser fijo):**
- -d “directorio”: path absoluto o relativo a analizar. Requerido.
- -o “directorio resultado”: path absoluto o relativo del directorio donde se generará el archivo de salida. Opcional. Si no se informa se generará en el directorio de ejecución.
- -u “umbral”: tamaño definido en KB para definir el umbral a analizar. Opcional. Si no es indicado, se considerará como umbral el promedio de peso de los archivos inspeccionados.
 
**Resultado esperado:**
1. Archivo de salida **resultado_{yyyy-mm-dd_hh:mm:ss}.out** indicando en primer lugar el resultado de los duplicados y luego el resultado de los archivos que superen el umbral.
2. Por consola se mostrará el mismo resultado generado en el archivo.
