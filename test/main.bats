#!/usr/bin/env bats

source "${BATS_TEST_DIRNAME}/../ejercicio3.sh" >/dev/null 2>/dev/null

MAIN_DIRECTORY=/tmp/directories/main
EXPECTED_DIRECTORY=./test/global/expected/main

print_output_vs_expected(){	
	RED='\033[0;31m'
	GREEN='\033[0;32m'
	NOCOLOR='\033[0m'

	output=$1
	expected_result=$2

	echo -e "${RED}Devuelve${NOCOLOR}\n"
        echo -e "$output\n\n"

        echo -e "${RED}Debería devolver${NOCOLOR}\n"
        echo -e "$expected_result\n"
}

# 1
@test "Returns file2 and file6 duplicated and file3 and file6 one time exceed threshold of ten" {
	directory=$MAIN_DIRECTORY/$BATS_TEST_NUMBER
	expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

	run main -d "$directory" -o /tmp -u 10

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 2
@test "Returns file2 duplicated and file8 triplicated and file1, file4 and file8 two times exceed threshold of average size" {
	directory=$MAIN_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run main -d "$directory" -o /tmp

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 3
@test "Returns file2 with space duplicated in dir1 with space and dir2 and file3 with space exceed threshold of average size" {
	directory=$MAIN_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run main -d "$directory" -o /tmp

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}
