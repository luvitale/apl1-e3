#!/bin/bash

get_help(){
	echo "Usage: $0 [isTemp]"
	echo "isTemp: <0/1>"
}

duplicated(){
	global_expected_directory="$1"
	mkdir $global_expected_directory/duplicated

	files=$(find ./test/expected/duplicated -name "*.txt")
	for file in $files
	do
		global_expected_file=$(cat $file |
			awk -F '\t' 'NF==3 {dir=$2; gsub("./test", "/tmp", dir); print $1"\t"dir"\t"$3} NF!=3 {print}')
	
		if [[ -z $1 && $1 = "1" ]]
		then
			echo "$global_expected_file" > ${file//.\/test/\/tmp}
		else
			echo "$global_expected_file" > ${file//.\/test/.\/test\/global}
		fi
	done
}

exceed_threshold(){
	global_expected_directory="$1"
	mkdir $global_expected_directory/exceed_threshold

	files=$(find ./test/expected/exceed_threshold -name "*.txt")
	for file in $files
	do
		global_expected_file=$(cat $file |
			awk -F '\t' 'NF==2 {dir=$1; gsub(".*/test/", "/tmp/", dir); print dir"\t"$2} NF!=2 {print}')
	
		if [[ -z $1 && $1 = "1" ]]
		then
			echo "$global_expected_file" > ${file//.\/test/\/tmp}
		else
			echo "$global_expected_file" > ${file//.\/test/.\/test\/global}
		fi
	done
}

main_tests(){
	global_expected_directory="$1"
	mkdir $global_expected_directory/main

	files=$(find ./test/expected/main -name "*.txt")
	for file in $files
	do
		global_expected_file=$(cat $file |
			awk -F '\t' 'NF==3 {
				dir=$2; gsub("./test", "/tmp", dir); print $1"\t"dir"\t"$3
			} NF==2 {
				dir=$1; gsub(".*/test/", "/tmp/", dir); print dir"\t"$2
			} NF!=2 && NF!=3 {
				print
		}')
	
		if [[ -z $1 && $1 = "1" ]]
		then
			echo "$global_expected_file" > ${file//.\/test/\/tmp}
		else
			echo "$global_expected_file" > ${file//.\/test/.\/test\/global}
		fi
	done
}

main(){
	if [[ -z $1 || $1 = "0" ]]
	then
		directory=./test/global
	elif [ $1 = "1" ]
	then
		directory=/tmp
	else
		get_help
		exit 1
	fi

	if [ -d $directory ] && [ $directory != "/tmp" ]
	then
		rm -Rf $directory
		mkdir $directory
	fi
	if [ -d $directory/expected ]; then rm -Rf $directory/expected; fi
	mkdir $directory/expected
	
	duplicated "$directory/expected"
	exceed_threshold "$directory/expected"
	main_tests "$directory/expected"
}

main $@
