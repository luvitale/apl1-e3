#!/bin/bash

FILE_DATE="2020-06-15"
FILE_HOUR="14"
FILE_MIN="00"

# Duplicated

## 1
returns_file1_duplicated_in_dir1_and_dir2(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory
	
	mkdir $test_directory/dir1
	mkdir $test_directory/dir2
	
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir2/file1.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir1/file2.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/file3.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir2/file4.txt
}

## 2
returns_file2_duplicated_in_dir1_dir2_and_dir3(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory
	
	mkdir $test_directory/dir1
	mkdir $test_directory/dir2
	mkdir $test_directory/dir3
	
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir1/file2.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir2/file2.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir3/file2.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/file3.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir1/file4.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir2/file5.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir3/file6.txt
}

## 3
returns_file2_duplicated_in_dir2_and_dir3_and_file6_duplicated_in_dir3_and_dir4(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        mkdir $test_directory/dir3
	mkdir $test_directory/dir4
        
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir2/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir3/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/file3.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/file4.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir2/file5.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir3/file6.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir4/file6.txt
}

## 4
returns_file1_with_space_duplicated_in_dir1_and_dir2(){
        test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory
	
	mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" "$test_directory/dir1/file1 with space.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" "$test_directory/dir2/file1 with space.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" "$test_directory/dir1/file2 with space.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" "$test_directory/dir1/file3 with space.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" "$test_directory/dir2/file4 with space.txt"
}

## 5
returns_file1_duplicated_in_dir1_dir2_and_subdir1(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

        mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        mkdir $test_directory/dir1/subdir1
        mkdir $test_directory/dir1/subdir2
	mkdir $test_directory/dir2/subdir3
        
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir2/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir1/subdir1/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/file3.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir2/file4.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir1/subdir2/file5.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir1/subdir2/file6.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" $test_directory/dir2/subdir3/file7.txt
}

## 6
returns_file1_duplicated_in_dir1_with_space_and_dir2_with_space(){
 	test_num=$2       
	test_directory=$1/$test_num
        mkdir $test_directory
        
	mkdir "$test_directory/dir1 with space"
        mkdir "$test_directory/dir2 with space"
        
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" "$test_directory/dir1 with space/file1.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" "$test_directory/dir2 with space/file1.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" "$test_directory/dir1 with space/file2.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" "$test_directory/dir1 with space/file3.txt"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" "$test_directory/dir2 with space/file4.txt"
}

## 7
returns_file1_duplicated_in_dir1_and_dir2_and_file1_with_space_duplicated_in_dir2_and_dir3(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir "$test_directory/dir1"
	mkdir "$test_directory/dir2"
	mkdir "$test_directory/dir3"

	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir2/file1
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" "$test_directory/dir2/file1 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" "$test_directory/dir3/file1 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/file2
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" "$test_directory/dir2/file2 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir3/file3
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" "$test_directory/dir3/file3 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" $test_directory/dir3/file4
}

## 8
returns_there_are_not_duplicated_files(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir "$test_directory/dir1"
	mkdir "$test_directory/dir2"
	mkdir "$test_directory/dir3"

	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir1/file2
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir2/file3
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/file4
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir3/file5
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir3/file6
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir2/file7
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir1/file8
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" $test_directory/dir2/file9
}

# Exceed threshold

## 1
returns_file1_file2_and_file6_exceed_threshold_of_ten(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	truncate -s 11KB $test_directory/file1
        truncate -s 22KB $test_directory/file2
        truncate -s 9KB $test_directory/file3
        truncate -s 2KB $test_directory/file4
        truncate -s 8KB $test_directory/file5
        truncate -s 11KB $test_directory/file6
        truncate -s 9KB $test_directory/file7
        truncate -s 7KB $test_directory/file8
        truncate -s 9KB $test_directory/file9
}

## 2
returns_file2_file4_and_file8_exceed_threshold_of_average_size(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	truncate -s 470KB $test_directory/file1
        truncate -s 510KB $test_directory/file2
        truncate -s 400KB $test_directory/file3
        truncate -s 700KB $test_directory/file4
        truncate -s 200KB $test_directory/file5
        truncate -s 480KB $test_directory/file6
        truncate -s 350KB $test_directory/file7
        truncate -s 900KB $test_directory/file8
        truncate -s 400KB $test_directory/file9
	truncate -s 450KB $test_directory/file10
}

## 3
returns_there_are_not_files_that_exceed_threshold_without_files(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory
}

# Main tests
## 1
returns_file2_and_file6_duplicated_and_file3_and_file6_one_time_exceed_threshold_of_ten(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        mkdir $test_directory/dir3
	mkdir $test_directory/dir4

	truncate -s 9KB $test_directory/dir1/file1.txt
        truncate -s 4KB $test_directory/dir2/file2.txt
        truncate -s 9KB $test_directory/dir3/file2.txt
        truncate -s 11KB $test_directory/dir1/file3.txt
        truncate -s 1KB $test_directory/dir1/file4.txt
        truncate -s 11KB $test_directory/dir3/file6.txt
        truncate -s 9KB $test_directory/dir4/file6.txt
        truncate -s 2KB $test_directory/dir1/file7.txt
        truncate -s 9KB $test_directory/dir3/file8.txt

 		
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir2/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir3/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/file3.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/file4.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir2/file5.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir3/file6.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir4/file6.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" $test_directory/dir1/file7.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:09" $test_directory/dir3/file8.txt
}

## 2
returns_file2_duplicated_and_file8_triplicated_and_file1_file4_file7_and_file8_two_times_exceed_threshold_of_average_size(){
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        mkdir $test_directory/dir1/subdir1
	mkdir $test_directory/dir1/subdir2
	mkdir $test_directory/dir2/subdir1
	mkdir $test_directory/dir2/subdir2

	truncate -s 510KB $test_directory/dir1/subdir1/file1.txt
        truncate -s 490KB $test_directory/dir1/subdir2/file2.txt
        truncate -s 450KB $test_directory/dir2/subdir2/file2.txt
        truncate -s 405KB $test_directory/dir1/subdir2/file3.txt
        truncate -s 555KB $test_directory/dir1/subdir1/file4.txt
        truncate -s 495KB $test_directory/dir1/file5.txt
        truncate -s 495KB $test_directory/dir1/subdir1/file6.txt
        truncate -s 501KB $test_directory/dir1/file7.txt
        truncate -s 595KB $test_directory/dir1/file8.txt
	truncate -s 505KB $test_directory/dir2/subdir1/file8.txt
	truncate -s 499KB $test_directory/dir2/subdir2/file8.txt

 		
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" $test_directory/dir1/subdir1/file1.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" $test_directory/dir1/subdir2/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" $test_directory/dir2/subdir2/file2.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" $test_directory/dir1/subdir2/file3.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" $test_directory/dir1/subdir1/file4.txt
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" $test_directory/dir1/file5.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" $test_directory/dir1/subdir1/file6.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" $test_directory/dir1/file7.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" $test_directory/dir1/file8.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:09" $test_directory/dir2/subdir1/file8.txt
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:10" $test_directory/dir2/subdir2/file8.txt
}

## 3
returns_file2_with_space_duplicated_in_dir1_with_space_and_dir2_and_file3_with_space_exceed_threshold_of_average_size() {
	test_num=$2
	test_directory=$1/$test_num
	mkdir $test_directory

	mkdir $test_directory/dir1
        mkdir $test_directory/dir2
        mkdir "$test_directory/dir1 with space"
	mkdir "$test_directory/dir2 with space"

	truncate -s 10KB "$test_directory/dir1/file1"
        truncate -s 10KB "$test_directory/dir1 with space/file1 with space"
        truncate -s 11KB "$test_directory/dir1 with space/file2 with space"
        truncate -s 10KB "$test_directory/dir2/file2 with space"
        truncate -s 11KB "$test_directory/dir1 with space/file3"
        truncate -s 20KB "$test_directory/dir1 with space/file3 with space"
        truncate -s 11KB "$test_directory/dir2 with space/file4"
        truncate -s 11KB "$test_directory/dir1/file4 with space"
        truncate -s 11KB "$test_directory/dir2 with space/file5"
	truncate -s 9KB "$test_directory/dir2/file5 with space"

 		
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:00" "$test_directory/dir1/file1"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:01" "$test_directory/dir1 with space/file1 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:02" "$test_directory/dir1 with space/file2 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:03" "$test_directory/dir2/file2 with space"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:04" "$test_directory/dir1 with space/file3"
        touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:05" "$test_directory/dir1 with space/file3 with space"
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:06" "$test_directory/dir2 with space/file4"
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:07" "$test_directory/dir1/file4 with space"
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:08" "$test_directory/dir2 with space/file5"
	touch -d "$FILE_DATE $FILE_HOUR:$FILE_MIN:09" "$test_directory/dir2/file5 with space"
}


duplicated(){
	test_num=1
	duplicated_directory=$1/duplicated
	mkdir $duplicated_directory

	returns_file1_duplicated_in_dir1_and_dir2 $duplicated_directory $test_num
	((test_num++))
	returns_file2_duplicated_in_dir1_dir2_and_dir3 $duplicated_directory $test_num
	((test_num++))
	returns_file2_duplicated_in_dir2_and_dir3_and_file6_duplicated_in_dir3_and_dir4 $duplicated_directory $test_num
	((test_num++))
	returns_file1_with_space_duplicated_in_dir1_and_dir2 $duplicated_directory $test_num
	((test_num++))
	returns_file1_duplicated_in_dir1_dir2_and_subdir1 $duplicated_directory $test_num
	((test_num++))
	returns_file1_duplicated_in_dir1_with_space_and_dir2_with_space $duplicated_directory $test_num
	((test_num++))
	returns_file1_duplicated_in_dir1_and_dir2_and_file1_with_space_duplicated_in_dir2_and_dir3 $duplicated_directory $test_num
	((test_num++))
	returns_there_are_not_duplicated_files $duplicated_directory $test_num
}

exceed_threshold(){
	test_num=1
	exceed_threshold_directory=$1/exceed_threshold
	mkdir $exceed_threshold_directory

	returns_file1_file2_and_file6_exceed_threshold_of_ten $exceed_threshold_directory $test_num
	((test_num++))
	returns_file2_file4_and_file8_exceed_threshold_of_average_size $exceed_threshold_directory $test_num
	((test_num++))
	returns_there_are_not_files_that_exceed_threshold_without_files $exceed_threshold_directory $test_num
}

main_tests(){
	test_num=1
	main_directory=$1/main
	mkdir $main_directory

	returns_file2_and_file6_duplicated_and_file3_and_file6_one_time_exceed_threshold_of_ten $main_directory $test_num
	((test_num++))
	returns_file2_duplicated_and_file8_triplicated_and_file1_file4_file7_and_file8_two_times_exceed_threshold_of_average_size $main_directory $test_num
	((test_num++))
	returns_file2_with_space_duplicated_in_dir1_with_space_and_dir2_and_file3_with_space_exceed_threshold_of_average_size $main_directory $test_num
}

main(){
	directory=./directories
	if [ -d "$DIRECTORY_WITH_TESTS" ]
	then
		directory="$DIRECTORY_WITH_TESTS/directories"
	fi
	if [ -d "$1" ]
	then
		directory="$1/directories"
	fi

	if [ -d $directory ]
	then
		rm -Rf $directory
	fi

	mkdir $directory
	
	duplicated $directory
	exceed_threshold $directory
	main_tests $directory

	echo "Finalizada creación de directorios para tests en $directory"
}

main "$@"
