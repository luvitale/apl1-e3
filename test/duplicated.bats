#!/usr/bin/env bats

source "${BATS_TEST_DIRNAME}/../ejercicio3.sh" >/dev/null 2>/dev/null

DUPLICATED_DIRECTORY=/tmp/directories/duplicated
EXPECTED_DIRECTORY=./test/global/expected/duplicated

print_output_vs_expected(){	
	RED='\033[0;31m'
	GREEN='\033[0;32m'
	NOCOLOR='\033[0m'

	output=$1
	expected_result=$2

	echo -e "${RED}Devuelve${NOCOLOR}\n"
        echo -e "output\n\n"

        echo -e "${RED}Debería devolver${NOCOLOR}\n"
        echo -e "expected_result\n"
}

# 1
@test "Returns file1 duplicated in dir1 and dir2" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
	expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

	run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 2
@test "Returns file2 duplicated in dir1, dir2 and dir3" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 3
@test "Returns file2 duplicated in dir2 and dir3 and file6 duplicated in dir3 and dir4" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 4
@test "Returns file1 with space duplicated in dir1 and dir2" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"
        
	[ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 5
@test "Returns file1 duplicated in dir1, dir2 and subdir1" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"
        
        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 6
@test "Returns file1 duplicated in dir1 with space and dir2 with space" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"
        
        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 7
@test "Returns file1 duplicated in dir1 and dir2 and file1 with space duplicated in dir2 and dir3" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	expected_result="$(cat $expected)"

        echo -e "Devuelve\n"
        echo -e "--------\n"
        echo -e "$output\n\n"

        echo -e "Debería devolver\n"
        echo -e "----------------\n"
        echo -e "$expected_result\n"	

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 8
@test "Returns there are not duplicated files" {
	directory=$DUPLICATED_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosDuplicados $directory

	print_output_vs_expected "$output" "$(cat $expected)"
        
        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}
