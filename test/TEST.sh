#!/bin/bash

duplicated(){
	echo "## Duplicated"
	echo -e "duplicated.bats\n"
	test_directory=./directories/duplicated
	expected_directory=./expected/duplicated
	dir_count=$(echo $test_directory/* | wc -w)
	for (( i=1; i <= $dir_count; ++i ))
	do
		tree $test_directory/$i
		echo
		cat $expected_directory/$i.txt
		if [ $i -ne $dir_count ]; then echo -e "\n"; fi
	done
}

exceed_threshold(){
	echo "## Exceed threshold"
	echo -e "exceed_threshold.bats\n"
	test_directory=./directories/exceed_threshold
	expected_directory=./expected/exceed_threshold
	dir_count=$(echo $test_directory/* | wc -w)
	for (( i=1; i <= $dir_count; ++i ))
	do
		tree $test_directory/$i
		echo
		cat $expected_directory/$i.txt
		if [ $i -ne $dir_count ]; then echo -e "\n"; fi
	done
}

main(){
	echo "# Tests de ejercicio 3 de APL 1" > TEST.md
	duplicated >> TEST.md
	exceed_threshold >> TEST.md
}

main
