#!/usr/bin/env bats

source "${BATS_TEST_DIRNAME}/../ejercicio3.sh" >/dev/null 2>/dev/null

THRESHOLD_DIRECTORY=/tmp/directories/exceed_threshold
EXPECTED_DIRECTORY=./test/global/expected/exceed_threshold

print_output_vs_expected(){
	RED='\033[0;31m'
        GREEN='\033[0;32m'
        NOCOLOR='\033[0m'

        output=$1
        expected_result=$2

        echo -e "${RED}Devuelve${NOCOLOR}\n"
        echo -e "$output\n\n"

        echo -e "${RED}Debería devolver${NOCOLOR}\n"
        echo -e "$expected_result\n"
}

# 9
@test "Returns file1, file2 and file6 exceed threshold of ten" {
	directory=$THRESHOLD_DIRECTORY/$BATS_TEST_NUMBER
	expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt
	umbral=10

	run archivosQueSuperanUmbral $directory $umbral

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 10
@test "Returns file2, file4 and file8 exceed threshold of average size" {
	directory=$THRESHOLD_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosQueSuperanUmbral $directory

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}

# 11
@test "Returns there are not files that exceed threshold without files" {
	directory=$THRESHOLD_DIRECTORY/$BATS_TEST_NUMBER
        expected=$EXPECTED_DIRECTORY/$BATS_TEST_NUMBER.txt

        run archivosQueSuperanUmbral $directory

	print_output_vs_expected "$output" "$(cat $expected)"

        [ "$status" -eq 0 ]
        [ "$output" = "$expected_result" ]
}
