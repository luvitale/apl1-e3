# Tests de ejercicio 3 de APL 1
## Duplicated
duplicated.bats

./directories/duplicated/1
├── dir1
│   ├── file1.txt
│   ├── file2.txt
│   └── file3.txt
└── dir2
    ├── file1.txt
    └── file4.txt

2 directories, 5 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file1.txt	./test/directories/duplicated/1/dir1	2020-06-15_14:00:00
file1.txt	./test/directories/duplicated/1/dir2	2020-06-15_14:00:01


./directories/duplicated/2
├── dir1
│   ├── file1.txt
│   ├── file2.txt
│   ├── file3.txt
│   └── file4.txt
├── dir2
│   ├── file2.txt
│   └── file5.txt
└── dir3
    ├── file2.txt
    └── file6.txt

3 directories, 8 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file2.txt	./test/directories/duplicated/2/dir1	2020-06-15_14:00:01
file2.txt	./test/directories/duplicated/2/dir2	2020-06-15_14:00:02
file2.txt	./test/directories/duplicated/2/dir3	2020-06-15_14:00:03


./directories/duplicated/3
├── dir1
│   ├── file1.txt
│   ├── file3.txt
│   └── file4.txt
├── dir2
│   ├── file2.txt
│   └── file5.txt
├── dir3
│   ├── file2.txt
│   └── file6.txt
└── dir4
    └── file6.txt

4 directories, 8 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file2.txt	./test/directories/duplicated/3/dir2	2020-06-15_14:00:01
file2.txt	./test/directories/duplicated/3/dir3	2020-06-15_14:00:02
		
file6.txt	./test/directories/duplicated/3/dir3	2020-06-15_14:00:06
file6.txt	./test/directories/duplicated/3/dir4	2020-06-15_14:00:07


./directories/duplicated/4
├── dir1
│   ├── file1 with space.txt
│   ├── file2 with space.txt
│   └── file3 with space.txt
└── dir2
    ├── file1 with space.txt
    └── file4 with space.txt

2 directories, 5 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file1 with space.txt	./test/directories/duplicated/4/dir1	2020-06-15_14:00:00
file1 with space.txt	./test/directories/duplicated/4/dir2	2020-06-15_14:00:01


./directories/duplicated/5
├── dir1
│   ├── file1.txt
│   ├── file2.txt
│   ├── file3.txt
│   ├── subdir1
│   │   └── file1.txt
│   └── subdir2
│       ├── file5.txt
│       └── file6.txt
└── dir2
    ├── file1.txt
    ├── file4.txt
    └── subdir3
        └── file7.txt

5 directories, 9 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file1.txt	./test/directories/duplicated/5/dir1	2020-06-15_14:00:00
file1.txt	./test/directories/duplicated/5/dir1/subdir1	2020-06-15_14:00:02
file1.txt	./test/directories/duplicated/5/dir2	2020-06-15_14:00:01


./directories/duplicated/6
├── dir1 with space
│   ├── file1.txt
│   ├── file2.txt
│   └── file3.txt
└── dir2 with space
    ├── file1.txt
    └── file4.txt

2 directories, 5 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file1.txt	./test/directories/duplicated/6/dir1 with space	2020-06-15_14:00:00
file1.txt	./test/directories/duplicated/6/dir2 with space	2020-06-15_14:00:01


./directories/duplicated/7
├── dir1
│   ├── file1
│   └── file2
├── dir2
│   ├── file1
│   ├── file1 with space
│   └── file2 with space
└── dir3
    ├── file1 with space
    ├── file3
    ├── file3 with space
    └── file4

3 directories, 9 files

Nombre del archivo duplicado	Directorios donde se encuentra	Fecha de modificación

file1	./test/directories/duplicated/7/dir1	2020-06-15_14:00:00
file1	./test/directories/duplicated/7/dir2	2020-06-15_14:00:01
		
file1 with space	./test/directories/duplicated/7/dir2	2020-06-15_14:00:02
file1 with space	./test/directories/duplicated/7/dir3	2020-06-15_14:00:03


./directories/duplicated/8
├── dir1
│   ├── file1
│   ├── file2
│   ├── file4
│   └── file8
├── dir2
│   ├── file3
│   ├── file7
│   └── file9
└── dir3
    ├── file5
    └── file6

3 directories, 9 files

No hay archivos duplicados.
## Exceed threshold
exceed_threshold.bats

./directories/exceed_threshold/1
├── file1
├── file2
├── file3
├── file4
├── file5
├── file6
├── file7
├── file8
└── file9

0 directories, 9 files

Path del archivo que supera umbral	Tamaño
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/1/file1	11kB
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/1/file2	22kB
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/1/file6	11kB


./directories/exceed_threshold/2
├── file1
├── file10
├── file2
├── file3
├── file4
├── file5
├── file6
├── file7
├── file8
└── file9

0 directories, 10 files

Path del archivo que supera umbral	Tamaño
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/2/file2	510kB
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/2/file4	700kB
/home/luvitale/apl1/ejercicio3/test/directories/exceed_threshold/2/file8	900kB


./directories/exceed_threshold/3

0 directories, 0 files

No hay archivos que superen el umbral.
